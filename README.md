# JavaTutorials

This is a collection of my first programs in Java.
They are intended as a remider to myself on how to do some simple things as well as a record of what I have done.
Most of the programs are based on exercizes in "Java - How to Program - Ninth Edition" by Paul and Harvey Deitel