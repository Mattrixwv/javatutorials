//Programs/Java/JavaTutorials/Draw.java
//Matthew Ellison
// Created: 02-12-19
//Modified: 02-12-19
//This is a program that simply draws a line from corner to corner in a window


import java.awt.Graphics;
import javax.swing.JPanel;


public class Draw extends JPanel{
	public void paintComponent(Graphics g){
		super.paintComponent(g);	//Ensures the panel displays correctly

		int width = getWidth();		//The width of the panel
		int height = getHeight();	//Get the height of the panel
		int stepLength = 15;	//Set the length each line will be separated by

		//Draw lines all the way around the panel, one step appart each time
		//Covers the bottom-left corner
		for(int cnt = 0;(cnt * stepLength) < width;++cnt){
			g.drawLine(0, (stepLength * cnt),  stepLength * (cnt + 1), height);
		}
		//Covers the bottom-right corner
		for(int cnt = 0;(cnt * stepLength) < width;++cnt){
			g.drawLine(cnt * stepLength, height, width, height - (stepLength * (cnt + 1)));
		}
		//Covers the top-right corner
		for(int cnt = 0;(cnt * stepLength) < width;++cnt){
			g.drawLine(width, height - (stepLength * cnt), width - (stepLength * (cnt + 1)), 0);
		}
		//Covers the top-left corner
		for(int cnt = 0;(cnt * stepLength) < width;++cnt){
			g.drawLine(width - (stepLength * cnt), 0, 0, stepLength * cnt);
		}
	}
}