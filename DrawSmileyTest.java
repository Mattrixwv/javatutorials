//Programs/Java/JavaTutorials/DrawSmiley.java
//Matthew Ellison
// Created: 02-12-19
//Modified: 02-12-19
//This class simply draws a smiley face


import javax.swing.JFrame;


public class DrawSmileyTest{
	public static void main(String[] argv){
		DrawSmiley panel = new DrawSmiley();
		JFrame application = new JFrame();

		application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		application.add(panel);
		application.setSize(230, 250);
		application.setVisible(true);
	}
}