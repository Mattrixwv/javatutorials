//Programs/Java/JavaTutorials/RandomIntegers.java
//Matthew Ellison
// Created: 02-11-19
//Modified: 02-11-19
//This is an example of a simple random number generator


import java.util.Random;


public class RandomIntegers{
	public static void main(String[] argv){
		Random generator = new Random();	//Create a random number generator
		//Generate 25, single digit, random numbers in 5 rows
		for(int row = 0;row < 5;++row){
			for(int col = 0;col < 5;++col){
				System.out.printf("%-3d", generator.nextInt(10));
			}
			System.out.println();	//Move to the next line
		}
	}
}