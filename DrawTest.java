//Programs/Java/JavaTutorials/Draw.java
//Matthew Ellison
// Created: 02-12-19
//Modified: 02-12-19
//This is a program that simply draws a line from corner to corner in a window


import javax.swing.JFrame;


public class DrawTest{
	public static void main(String[] argv){
		Draw panel = new Draw();

		//Create a new frame to hold the panel
		JFrame application = new JFrame();

		//Set the frame to exit when it is closed
		application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		application.add(panel);	//Add the panel to the frame
		application.setSize(500, 500);	//Set the size of the frame
		application.setVisible(true);	//Make the frame visible
	}
}