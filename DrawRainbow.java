//Java/JavaTutorials/DrawRainbow.java
//Matthew Ellison
// Created: 02-28-19
//Modified: 02-28-19
//This program draws a rainbow on a panel


import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;


public class DrawRainbow extends JPanel{
	//Define some extra colors
	private static final Color VIOLET = new Color(128, 0, 128);
	private static final Color INDIGO = new Color(75, 0, 130);
	private static final Color BACKGROUND_COLOR = Color.WHITE;

	//For the colors used in the rainbow. The two background colors represent a blank space
	private static final Color[] colors = {BACKGROUND_COLOR, BACKGROUND_COLOR, VIOLET, INDIGO, Color.BLUE, Color.GREEN, Color.YELLOW, Color.ORANGE, Color.RED};

	public DrawRainbow(){
		setBackground(BACKGROUND_COLOR);
	}

	//Draws a rainbow using concentric arcs
	public void paintComponent(Graphics g){
		super.paintComponent(g);

		int radius = 100;	//The radius of an arc

		//Draw the rainbow near the bottom-center
		int centerX = getWidth() / 2;
		int centerY = getHeight() - 10;

		//Draws filled arcs starting with the outermost
		for(int cnt = colors.length;cnt > 0;--cnt){
			//Set the color for the current arc
			g.setColor(colors[cnt - 1]);

			//Fill the arc from 0 to 180 degrees
			g.fillArc(centerX - cnt * radius, centerY - cnt * radius, cnt * radius * 2, cnt * radius * 2, 0, 180);
		}
	}
}