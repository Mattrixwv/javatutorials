//Java/JavaTutorials/Array.java
//Matthew Ellison
// Created: 02-28-19
//Modified: 02-28-19
//This file contains some practice using different kinds of arrays in java

import java.util.Random;
import java.util.Arrays;
import java.util.ArrayList;


public class Array{
	private static final int NUMS_TO_ROLL = 10000;
	private static final int HIGHEST_NUM = 6;
	public static void main(String[] argv){
		Random generator = new Random();
		//Roll some random numbers and store the results in an array

//		int[] numsAry = {0, 0, 0, 0, 0, 0};
		int[] numsAry = new int[HIGHEST_NUM + 1];
		Arrays.fill(numsAry, 0);
/*
		for(int cnt = 0;cnt < HIGHEST_NUM;++cnt){
			numsAry[cnt] = 0;
		}
*/
		System.out.println("Rolling numbers for the regular array...");
		for(int cnt = 0;cnt < NUMS_TO_ROLL;++cnt){
			++numsAry[generator.nextInt(HIGHEST_NUM)];
		}
		//Print the results
		System.out.println("Results:");
		for(int numRolled = 0;numRolled < HIGHEST_NUM;++numRolled){
			System.out.printf("%d. %d\n", numRolled + 1, numsAry[numRolled]);
		}


		//Roll some random numbers and store the results in a dynamic array
		System.out.println("\n\nRolling numbers for the dynamic array...");
		ArrayList<Integer> numsList = new ArrayList<Integer>();
		for(int cnt = 0;cnt <= HIGHEST_NUM;++cnt){
			numsList.add(0);
		}
		for(int cnt = 0;cnt < NUMS_TO_ROLL;++cnt){
			int num = generator.nextInt(HIGHEST_NUM);
			numsList.set(num, numsList.get(num) + 1);
		}
		//Print the results
		for(int numRolled = 0;numRolled < HIGHEST_NUM;++numRolled){
			System.out.printf("%d. %d\n", numRolled + 1, numsList.get(numRolled));
		}
		System.out.println("Test completed");
	}
}

/* Results:

*/
