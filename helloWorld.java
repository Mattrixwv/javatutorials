//Java/JavaTutorials/helloWorld.java
//Matthew Ellison
// Created: 02-11-19
//Modified: 02-11-19
//This is a simple hello world program written in java

import java.util.Scanner;

public class helloWorld{
	public static void main(String[] argv){
		String hello = "Hello" + ' ' +  "World";
		//Print a static string
		System.out.println("Hello World");
		//Print a variable string
		System.out.printf("%s again\n", hello);
		//Add a number to a string
		int num = 2;
		String hello2 = hello;
		hello2 += ' ';
		hello2 += num;	//Apparently an automatic conversion is done
		System.out.println(hello2);
		//Get input and use it to print a message
		java.util.Scanner input = new Scanner(System.in);
		System.out.print("What number do you want to print? ");
		num = input.nextInt();
		input.close();
		hello2 = hello + ' ' + num;
		System.out.println(hello2);
	}
}

/* Results:
Hello World
Hello World again
Hello World 2
What number do you want to print? 15
Hello World 15
*/