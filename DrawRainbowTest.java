//Java/JavaTutorials/DrawRainbowTest.java
//Matthew Ellison
// Created: 02-28-19
//Modified: 02-28-19
//This program runs a test of DrawRainbow


import javax.swing.JFrame;


public class DrawRainbowTest{
	public static void main(String[] argv){
		DrawRainbow panel = new DrawRainbow();
		JFrame application = new JFrame();

		application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		application.add(panel);
		application.setSize(1920, 1080);
		application.setVisible(true);
	}
}