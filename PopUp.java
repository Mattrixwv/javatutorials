//Java/JavaTutorials/PopUp.java
//Matthew Ellison
// Created: 02-12-19
//Modified: 02-12-19
//This is a simple tutorial on pop up boxes


import javax.swing.JOptionPane;

public class PopUp{
	public static void main(String[] argv){
		//Prompt the user to enter a string
		String words = JOptionPane.showInputDialog("Enter a string to be printed");
		JOptionPane.showMessageDialog(null, words);
		//Prompt the user to enter 2 numbers and print their sum
		words = JOptionPane.showInputDialog("Enter a number to be summed");
		int num1 = Integer.parseInt(words);
		words = JOptionPane.showInputDialog("Enter another number to be summed");
		int num2 = Integer.parseInt(words);
		JOptionPane.showMessageDialog(null, num1 + " + " + num2 + " = " + (num1 + num2));
	}
}

/* Results:

*/
