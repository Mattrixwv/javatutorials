//Programs/Java/JavaTutorials/Shapes.java
//Matthew Ellison
// Created: 02-12-19
//Modified: 002-12-19
//Demonstration of drawing rectangles and ovals


import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class ShapesTest{
	public static void main(String[] argv){
		//Get the user's choice
		String input = JOptionPane.showInputDialog("Enter 1 to draw rectangles\nEnter2 to draw ovals");
		//Convert the input to an int
		int choice = Integer.parseInt(input);
		//Create the panel with the shapes drawn on it
		Shapes panel = new Shapes(choice);
		//Create a new frame
		JFrame application = new JFrame();

		application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		application.add(panel);
		application.setSize(300, 300);
		application.setVisible(true);
	}
}