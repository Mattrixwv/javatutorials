//Programs/Java/JavaTutorials/Shapes.java
//Matthew Ellison
// Created: 02-12-19
//Modified: 002-12-19
//Demonstration of drawing rectangles and ovals
//Ovals work very similarly to rectanges. In the program it draws a bounding rectangle then draws the oval within it


import java.awt.Graphics;
import javax.swing.JPanel;


public class Shapes extends JPanel{
	private int choice;	//The choice of what shape to draw
	//Constructor
	public Shapes(int userChoice){
		choice = userChoice;
	}
	//Draws the shapes
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		for(int cnt = 0;cnt < 10;++cnt){
			//Pick a shape based on what the user chose
			switch(choice){
				case 1: g.drawRect(10 + (cnt * 10), 10 + (cnt * 10), 50 + (cnt * 10), 50 + (cnt * 10)); break;
				case 2: g.drawOval(10 + (cnt * 10), 10 + (cnt * 10), 50 + (cnt * 10), 50 + (cnt * 10)); break;
			}
		}
	}
}